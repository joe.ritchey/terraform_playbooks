I'm going to focus on the VPC networking side because this seemed to be where a lot of confusion exists. There is also an opportunity for savings, (or cost overruns depending on how you look at it). Since I am going to be focusing on a development environment, I am going to make a few assumptions from the start:
Everything in this environment can be deleted and recreated at any time.
High availability, high reliability is not a concern. 
Still adhere to best security practices, like not making databases publicly accessible. 

What we are going to setup
A new VPC with a CIDR range of 172.16.0.0/24
Four subnets total
Two public subnets in us-east-1a and us-east-1b
Two private subnets in us-east-1a and us-east-1b
A Internet Gateway attached as the default route (0.0.0.0/0) 
VPC endpoints for s3, dynamodb (this are the only gateway endpoints available at the time of this writing)
1 EC2 instance in the public subnet of us-east-1b to use as a NAT Gateway/Bastion host. 

Why 4 subnets? Subnets do not cost anything to have and if you want to run RDS you need two subnets. 
First of course is the VPC. Everything goes into the VPC so this is first. Then we will take the VPC ID from this install and use it all over the rest of this. 
Oh, and let's not forget tags, tags, tags! Cloud tracking and managing projects are all about the tags. Tagging all day every day. 

Some help from my friends:
I'm going to use the https://github.com/terraform-aws-modules/terraform-aws-vpc to build the VPC. 
For calculating subnets the online subnet calculator- www.subnet-calculator.com/

Using one EC2 instance for both NAT and jump host traffic:
1 - this is not the most secure thing
2 - Not the most HA-DR thing either
This is a development environment so we are here to save a few bucks
Also, you shouldn't be dealing with Prod data in your dev account DUR. 

As I was building this out I found it takes a lot more terraform code to build out one EC2 instance than it did to build the VPC. EC2 instances needs a lot of configuration to get just one started. We need a security group, AMI id, SSH key. Wait SSH Key what is this 2014. Skipping the SSH keys here and I'm going right to SSM Session manager. So no SSH key, but we will need a IAM Policy to attached to the EC2 instance. My goal here is also not to build clusters of EC2 instances but something I can build and destroy over and over again. 

Subnets Here is what I worked out for 4 subnets in a /24 CIDR range
172.16.0.0 - 172.16.0.255

172.16.0.0 - 172.16.0.63 /26 -> 172.16.0.1 - 172.16.0.62
172.16.0.64 - 172.16.0.127  /26 -> 172.16.0.65 - 172.16.0.126
172.16.0.128 - 172.16.0.191   /26 -> 172.16.0.129 - 172.16.0.190
172.16.0.192 - 172.16.0.255   /26 -> 172.16.0.193 - 172.16.0.254
