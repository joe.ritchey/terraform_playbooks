
variable "aws_region" {
  description = "The region the VPC is going to be deployed to"
  type        = string
  default     = "us-east-2"
}

variable "vpc_name" {
  description = "Name to be used on all the resources as identifier"
  type        = string
  default     = "development"
}

variable "nat_instance_ami" {
    description = "AMI ID of the NAT instance to use. ami-01ef31f9f39c5aaed is a instance provided by AWS"
    type        = string
    default     = "ami-01ef31f9f39c5aaed"
}

variable "nat_instance_type" {
    description = "Instance type for the NAT instance"
    type        = string
    default     = "t3.micro"
}
