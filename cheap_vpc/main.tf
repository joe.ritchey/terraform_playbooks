# Using the AWS VPC modules https://github.com/terraform-aws-modules/terraform-aws-vpc 
# I cheat calculating subnets - https://www.subnet-calculator.com/
# 
provider "aws" {
    region = var.aws_region
    profile = "default"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = var.vpc_name

  # For this first pass I'm going to leave the CIDR range of the vpc and the
  # number of AZ and CIDR ranges of those AZ hardcoded 
  # TODO: use the region to determine the AZs so this doesn't have to be 
  # statically assigned
  cidr = "172.16.0.0/24"
  azs             = ["us-east-2a", "us-east-2b"]
  private_subnets = ["172.16.0.0/26", "172.16.0.64/26"]
  public_subnets  = ["172.16.0.128/26", "172.16.0.192/26"]

  enable_nat_gateway = false
  enable_vpn_gateway = false

  tags = {
    Terraform = "true"
    environment = "dev"
  }
}


# Create a EIP to attach to the NAT EC2 instance
resource "aws_eip" "nat" {
  count = 1

  vpc = true
  tags = {
    Terraform = "true"
    environment = "dev"
    Name = "nat_eip"
  }
}

# After the VPC and network are setup create a ecs instance for NAT gateway
# Just starting a EC2 instance sounds easy

module "security_group" {
  source  = "terraform-aws-modules/security-group/aws"

  name        = "NAT-SG"
  description = "Security group for usage with NAT EC2 instance"
  vpc_id      = module.vpc.vpc_id

  ingress_cidr_blocks = ["172.16.0.0/24"]
  ingress_rules = ["http-80-tcp", "https-443-tcp"]
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

module "ec2" {
  source                 = "terraform-aws-modules/ec2-instance/aws"
  name                   = "nat-instance"
  instance_count         = 1
  ami                    = var.nat_instance_ami
  instance_type          = var.nat_instance_type
  cpu_credits            = "unlimited"
  subnet_id              = tolist(module.vpc.public_subnets)[0]
  vpc_security_group_ids = [module.security_group.this_security_group_id]
  associate_public_ip_address = true
  source_dest_check = false
  tags = {
    Terraform   = "true"
    Environment = "dev"
  }
}

# Now will use the NAT instance a the default route for
# the private subnets

resource "aws_route" "nat_routing" {
  count = length(module.vpc.private_route_table_ids)
  route_table_id = element(module.vpc.private_route_table_ids, count.index)
  destination_cidr_block    = "0.0.0.0/0"
  instance_id = module.ec2.id[0]
}